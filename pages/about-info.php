<?php
require_once "../PHP/databaseConnection.php";
include "../PHP/functions.php";
include "../PHP/forumAPI.php";
session_start();


?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>forum title - Info</title>
    <link rel="shortcut icon" type="image/x-icon" href="resources/META/favicon.png" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../CSS/header.css">
    <link rel="stylesheet" href="../CSS/index.css">
    <script type = "text/javascript" src = "../JS/functions.js" ></script>
    <script type = "text/javascript" src = "../JS/changeTheme.js" ></script>
  </head>
  <body>
    <header>
      <a href="/index.php"><img src="/resources/META/banner.png" alt="logo"></a>
      <form id="search-box" action="PHP/search.php" method="post">
        <div class="search-wrapper">
          <input type="text" autocomplete="off" name="Search" placeholder="Search...">
          <img src="/resources/icons/magnifying-glass.svg" alt="magnifying-glass-icon">
        </div>
      </form>
    </header>
    <nav>
      <div class="navbar">
        <div class="main-nav">
          <div class="main-nav-left">
            <li>
              <div class="nav-element">
                <span><a href="/index.php">Forums</a></span>
              </div>
            </li>
            <li>
              <div class="nav-element" id="active">
                <span><a href="/pages/about-info.php">About</a></span>
              </div>
            </li>
          </div>

          <div class="main-nav-right">

            <li>
              <?php
                if (isset($_SESSION['username'])) {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"profile-img\");' onmouseout='unhover(\"profile-img\");' onClick='window.location.href=\"/pages/user_account.php\";'>";
                  echo  "<img src='/resources/icons/profile.svg' alt='profile-icon' id='login-img' >";
                  echo  "<a href='#'>". $_SESSION['username'] ."</a>";
                  echo "</div>";
                }
                else {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"login-img\");' onmouseout='unhover(\"login-img\");' onClick='window.location.href=\"/PHP/login.php\";'>";
                  echo  "<img src='/resources/icons/key.svg' alt='key-icon' id='login-img' >";
                  echo  "<a href='#'>Login</a>";
                  echo "</div>";
                }
              ?>
            </li>

            <li>
              <?php
               $_SESSION['previousURL']= $_SERVER['REQUEST_URI'];
               if (isset($_SESSION['username'])) {
                 echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"logout-img\");' onmouseout='unhover(\"logout-img\");' onClick='window.location.href=\"/PHP/logout.php\";'>";
                 echo  "<img src='/resources/icons/door.svg' alt='door-icon' id='logout-img' >";
                 echo  "<a href='#'>Logout</a>";
                 echo "</div>";
               }
                else {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"register-img\");' onmouseout='unhover(\"register-img\");' onClick='window.location.href=\"/PHP/register.php\";'>";
                  echo  "<img src='/resources/icons/clipboard.svg' alt='clipboard-icon' id='register-img' >";
                  echo  "<a href='#'>Register</a>";
                  echo "</div>";
                }
              ?>
            </li>
          </div>
        </div>
        <div class="sub-nav">
          <ul>
            <li>
              <div class="nav-element" id="active">
                <span><a href="/pages/about-info.php">Info</a></span>
              </div>
            </li>
          </ul>
        </div>
      </nav>

        <div class="page-wrapper">
          <div class="status-indicators">
            <div class="current-page-indicator">
              <a href="../index.php">Forums</a>
              <a href="#"> > </a>
              <a href="javascript:window.location.href=window.location.href" style="color:white;">About</a>
            </div>
          </div>
          <div class="page-wrapper-left">
            <div class="page-left-content page-board1">
              <div class="page-left-content-title">
                <a>Forum Rules</a>
              </div>
              <div class="page-left-content-wrapper">
                <div class = "information-box">
                  <p>1. No Spam / Advertising / Self-promote in the forums
                  <p>These forums define spam as unsolicited advertisement for goods, services and/or other web sites, or posts with little, or completely unrelated content. Do not spam the forums with links to your site or product, or try to self-promote your website, business or forums etc.
                    Spamming also includes sending private messages to a large number of different users.</p>

                  <p>DO NOT ASK for email addresses or phone numbers</p>

                  <p>Your account will be banned permanently and your posts will be deleted.</p>

                  <p>2. Do not post copyright-infringing material
                    Providing or asking for information on how to illegally obtain copyrighted materials is forbidden.</p>

                  <p>3. Do not post “offensive” posts, links or images
                    Any material which constitutes defamation, harassment, or abuse is strictly prohibited. Material that is sexually or otherwise obscene, racist, or otherwise overly discriminatory is not permitted on these forums. This includes user pictures. Use common sense while posting.
                    This is a web site for accountancy professionals.</p>

                  <p>4. Do not cross post questions
                    Please refrain from posting the same question in several forums. There is normally one forum which is most suitable in which to post your question.</p>

                  <p>5. Do not PM users asking for help
                    Do not send private messages to any users asking for help. If you need help, make a new thread in the appropriate forum then the whole community can help and benefit.</p>

                  <p>6. Remain respectful of other members at all times
                      All posts should be professional and courteous. You have every right to disagree with your fellow community members and explain your perspective.</p>

                  <p>However, you are not free to attack, degrade, insult, or otherwise belittle them or the quality of this community. It does not matter what title or power you hold in these forums, you are expected to obey this rule.</p>
                </div>
              </div>
            </div>

          </div>
          <div class="page-wrapper-right">
            <div class="page-right-content questionOfTheWeek">
              <div class="page-right-content-title">
                <a>Staff</a>
              </div>
              <div class="page-right-content-wrapper">

              </div>
            </div>
            <div class="page-right-content recentActivity">
              <div class="page-right-content-title">
                <a href="#">Recent Activity</a>
              </div>
              <div class="page-right-content-wrapper">

              </div>
            </div>
          </div>
        </div>

  </body>
</html>
