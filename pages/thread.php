<?php
require_once "../PHP/databaseConnection.php";
require_once "../PHP/functions.php";
require_once "../PHP/forumAPI.php";
session_start();

$boardName = null;
$subforumName = null;
$threadTitle = null;
$threadID = null;

if(isset($_GET['threadId'])) {
  $threadID = $_GET['threadId'];
}

if(isset($_GET['subforum'])) {
  $subforumName = $_GET['subforum'];

  $sql = "SELECT threads.title, subforums.parentForum FROM threads LEFT JOIN subforums ON threads.parentSubforum=subforums.name WHERE id=".$threadID."";

  if ($result = mysqli_query($conn, $sql)) {
    if (mysqli_num_rows($result) > 0) {
      $row = mysqli_fetch_assoc($result);
      $threadTitle = $row['title'];
      $boardName = $row['parentForum'];

      mysqli_free_result($result);
    }
    else {
      echo "Your SQL query did not match any existing objects in database.";
    }
  }
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>forum title - home</title>
    <link rel="shortcut icon" type="image/x-icon" href="/resources/META/favicon.png" />
    <!-- <link rel="stylesheet" href="/CSS/header.css"> -->
    <link rel="stylesheet" href="/CSS/thread.css">
    <script type = "text/javascript" src = "../JS/changeTheme.js" ></script>
  </head>
  <body>
    <header>
      <a href="/index.php"><img src="/resources/META/banner.png" alt="logo"></a>
      <form id="search-box" action="PHP/search.php" method="post">
        <div class="search-wrapper">
          <input type="text" autocomplete="off" name="Search" placeholder="Search...">
          <img src="/resources/icons/magnifying-glass.svg" alt="magnifying-glass-icon">
        </div>
      </form>
    </header>
    <nav>
      <div class="navbar">
        <div class="main-nav">
          <div class="main-nav-left">
            <li>
              <div class="nav-element" id="active">
                <span><a href="/index.php">Forums</a></span>
              </div>
            </li>
            <li>
              <div class="nav-element">
                <span><a href="/pages/about-info.php">About</a></span>
              </div>
            </li>
          </div>

          <div class="main-nav-right">

            <li>
              <?php
                if (isset($_SESSION['username'])) {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"login-img\");' onmouseout='unhover(\"login-img\");' onClick='window.location.href=\"user_account.php\";'>";
                  echo  "<img src='/resources/icons/key.svg' alt='key-icon' id='login-img' >";
                  echo  "<a href='#'>". $_SESSION['username'] ."</a>";
                  echo "</div>";
                }
                else {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"login-img\");' onmouseout='unhover(\"login-img\");' onClick='window.location.href=\"/PHP/login.php\";'>";
                  echo  "<img src='/resources/icons/key.svg' alt='key-icon' id='login-img' >";
                  echo  "<a href='#'>Login</a>";
                  echo "</div>";
                }
              ?>
            </li>

            <li>
              <?php
                if (isset($_SESSION['username'])) {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"register-img\");' onmouseout='unhover(\"register-img\");' onClick='window.location.href=\"/PHP/logout.php\";'>";
                  echo  "<img src='/resources/icons/clipboard.svg' alt='clipboard-icon' id='register-img' >";
                  echo  "<a href='#'>Logout</a>";
                  echo "</div>";
                }
                else {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"register-img\");' onmouseout='unhover(\"register-img\");' onClick='window.location.href=\"/PHP/register.php\";'>";
                  echo  "<img src='/resources/icons/clipboard.svg' alt='clipboard-icon' id='register-img' >";
                  echo  "<a href='#'>Register</a>";
                  echo "</div>";
                }
              ?>
            </li>
          </div>
        </div>
        <div class="sub-nav">
          <ul>
            <li>
              <div class="nav-element">
                <span><a href="/index.php">Home</a></span>
              </div>
            </li>
            <li>
              <div class="nav-element" id="active">
                <div class="dropdown">
                  <a href="/pages/forum.php?forum=all&sorting=popular&page=1">Popular</a>
                </div>
              </div>
            </li>
            <li>
              <div class="nav-element">
                <div class="dropdown">
                  <a href="/pages/forum.php?forum=all&sorting=new&page=1">New</a>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </nav>

    <div class="page-wrapper">
      <div class="status-indicators">
        <div class="current-page-indicator">
          <a href="../index.php">Forums</a>
          <a href="#"> > </a>
          <a href="forum.php?forum=<?php echo $boardName; ?>&sorting=popular"> <?php echo $boardName; ?> </a>
          <a href="#"> > </a>
          <a href="subforum.php?subforum=<?php echo $subforumName; ?>&sorting=popular&page=1"> <?php echo $subforumName; ?> </a>
          <a href="#"> > </a>
          <a href="javascript:window.location.href=window.location.href" style="color:white;"> <?php echo $threadTitle; ?> </a>
        </div>
        <div class="current-subpage-indicator">
          <a href="#">1, 2, 3...10</a>
        </div>
      </div>

      <div class="main-box">
        <div class="main-box-content">

          <!-- OP userinfo and OP first comment -->
          <?php
          $threads[] = array(
            'username' => null,
            'privilege' => null,
            'registerDate' => null,
            'title' => null,
            'comment' => null,
            'date' => null,
          );

          $sql = "SELECT userbase.username, userbase.privilege, userbase.registerDate, threads.title, threads.comment, threads.date
                  FROM userbase LEFT JOIN threads
                  ON userbase.username=threads.creator
                  WHERE id=".$threadID."";

          if ($result = mysqli_query($conn, $sql)) {
            if (mysqli_num_rows($result) > 0) {
              $i = 0;
              while ($row = mysqli_fetch_assoc($result)) {
                $threads[$i]['username'] = $row['username'];
                $threads[$i]['privilege'] = $row['privilege'];
                $threads[$i]['registerDate'] = $row['registerDate'];
                $threads[$i]['title'] = $row['title'];
                $threads[$i]['comment'] = $row['comment'];
                $threads[$i]['date'] = $row['date'];
                $i++;
              }
              mysqli_free_result($result);
            }
            else {
              echo "Your SQL query did not match any existing objects in database.";
            }
          }
          ?>

          <div class='main-box-content-element'>
            <div class='main-box-content-element-status'>
              <div class='main-box-content-element-status-userImage'>
                <img src="data:image/jpeg;base64,<?php echo getUserProfileImage($conn, $threads[0]['username']); ?>" width="200px"/>
              </div>
              <div class='main-box-content-element-status-userInfo'>
                <p><?php echo $threads[0]['username'] ?></p>
                <p><?php echo $threads[0]['privilege'] ?></p>
                <p><?php echo $threads[0]['registerDate'] ?></p>
                <p>OP SCORE HERE</p>
              </div>
            </div>
            <div class='main-box-content-element-main'>
              <div id='container'>
                <p class="title"><?php echo $threads[0]['title'] ?></p>
                <pre class="latest-reply"><?php echo $threads[0]['comment'] ?></pre>
              </div>
            </div>
          </div>

          <!-- Textbox if logged in -->
          <?php
          if (isset($_SESSION['username'])) {
            if (authenticateUser($conn, $_SESSION['username'])) {
              echo "<div class='textBox'>";
              echo   "<form action='thread.php?subforum=" . $subforumName . "&threadId=" . $threadID . "' method='POST' enctype='multipart/form-data'>";
              echo     "<textarea name='content' placeholder='Write a message'></textarea>";
              echo     "<input type='submit' name='submit' value='Send'></input>";
              echo   "</form>";
              echo "</div>";
            }

            if (isset($_POST['content'])) {
              $getId = mysqli_query($conn, "SELECT * FROM posts");
              $id = mysqli_num_rows($getId) + 1;
              $content = mysqli_real_escape_string($conn, $_POST['content']);
              $username = mysqli_real_escape_string($conn, $_SESSION['username']);
              $date = date("Y-m-d H:i:s");

              $sql = $sql = "CALL createPost('$id', '$content', '$username', '$threadID', '$date')";
              mysqli_query($conn, $sql);
            }
          }
          else {
            echo "<div class='textBox'>";
            echo   "<div class='container'>";
            echo     "<textarea name='content' placeholder='Write a message' rows='10' cols='100'></textarea>";
            echo     "<a href='../PHP/login.php' class='loginBtn'>";
            echo       "<div class='loginBtn-text'>Login</div>";
            echo     "</a>";
            echo   "</div>";
            echo "</div>";
          }
          ?>

          <!-- All comments in thread -->
          <?php
          $threads[] = array(
            'username' => null,
            'privilege' => null,
            'registerDate' => null,
            'comment' => null,
            'date' => null,
          );

          $sql = "SELECT userbase.username, userbase.privilege, userbase.registerDate, posts.comment, posts.date
                  FROM userbase LEFT JOIN posts
                  ON userbase.username=posts.creator
                  WHERE posts.parentThread=".$threadID."
                  ORDER BY date ASC";

          if ($result = mysqli_query($conn, $sql)) {
            if (mysqli_num_rows($result) > 0) {
              $i = 0;
              while ($row = mysqli_fetch_assoc($result)) {
                echo "<div class='thread-box-content-element'>";
                echo  "<div class='thread-box-content-element-status'>";
                echo   "<div class='thread-box-content-element-status-userImage'>";
                echo    "<img src='data:image/jpeg;base64," . getUserProfileImage($conn, $row['username']) . "' width='160px'/>"; //Image here
                echo   "</div>";
                echo    "<div class='thread-box-content-element-status-userInfo'>";
                echo     "<p>" . $row['username'] . "</p>";
                echo     "<p>" . $row['privilege'] . "</p>";
                echo     "<p>" . $row['registerDate'] . "</p>";
                echo    "</div>";
                echo  "</div>";
                echo  "<div class='thread-box-content-element-main'>";
                echo    "<div id='user-comment'>";
                echo      " <pre class='latest-reply'>" . $row['comment'] . "</pre>";
                echo    "</div>";
                echo    "<div id='user-postdate'>";
                echo      "<p>Date: " . $row['date'] . "</p>";
                echo    "</div>";
                echo    "<div id='user-like-comment'>";
                echo      "<p class='likes'><span>like</span> <span>dislike</span></p>";
                echo    "</div>";
                echo  "</div>";
                echo "</div>";
                }
              mysqli_free_result($result);
            }
          }
          ?>
        </div>
      </div>
    </div>

  </body>
</html>
