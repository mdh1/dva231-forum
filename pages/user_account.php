<?php
require_once ("../PHP/functions.php");
require_once ("../PHP/databaseConnection.php");
require_once ("../PHP/forumAPI.php");
session_start();

$username= $_SESSION['username'];

$sql= "SELECT * FROM userbase WHERE username = '".$username."'";
$result=  $conn->query($sql);
$userInfo = array (
  'username' => null,
  'email' => null,
);

if ($result->num_rows > 0) {
  // output data of each row
      $row = mysqli_fetch_assoc($result);
      $userInfo['username']=$row['username'];
      $userInfo['email']=$row['email'];
  }
else {
  echo "0 results";
}


 ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>forum title - home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="shortcut icon" type="image/x-icon" href="resources/META/favicon.png" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
     <link rel="stylesheet" href="/CSS/index.css">
     <link rel="stylesheet" href="/CSS/header.css">
    <link rel="stylesheet" href="/CSS/user_account.css">

    <script type = "text/javascript" src = "/JS/functions.js" ></script>
  </head>
  <body>
    <header>
      <a href="/index.php"><img src="/resources/META/banner.png" alt="logo"></a>
      <form id="search-box" action="PHP/search.php" method="post">
        <div class="search-wrapper">
          <input type="text" autocomplete="off" name="Search" placeholder="Search...">
          <img src="/resources/icons/magnifying-glass.svg" alt="magnifying-glass-icon">
        </div>
      </form>
    </header>
    <nav>
      <div class="navbar">
        <div class="main-nav">
          <div class="main-nav-left">
            <li>
              <div class="nav-element">
                <span><a href="/index.php">Forums</a></span>
              </div>
            </li>
            <li>
              <div class="nav-element">
                <span><a href="/pages/about-info.php">About</a></span>
              </div>
            </li>
          </div>

          <div class="main-nav-right">

            <li>
              <?php
                if (isset($_SESSION['username'])) {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"profile-img\");' onmouseout='unhover(\"profile-img\");' onClick='window.location.href=\"/pages/user_account.php\";'>";
                  echo  "<img src='/resources/icons/profile.svg' alt='profile-icon' id='login-img' >";
                  echo  "<a href='#'>". $_SESSION['username'] ."</a>";
                  echo "</div>";
                }
                else {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"login-img\");' onmouseout='unhover(\"login-img\");' onClick='window.location.href=\"/PHP/login.php\";'>";
                  echo  "<img src='/resources/icons/key.svg' alt='key-icon' id='login-img' >";
                  echo  "<a href='#'>Login</a>";
                  echo "</div>";
                }
              ?>
            </li>

            <li>
              <?php
                if (isset($_SESSION['username'])) {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"logout-img\");' onmouseout='unhover(\"logout-img\");' onClick='window.location.href=\"/PHP/logout.php\";'>";
                  echo  "<img src='/resources/icons/door.svg' alt='door-icon' id='logout-img' >";
                  echo  "<a href='#'>Logout</a>";
                  echo "</div>";
                }
                else {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"register-img\");' onmouseout='unhover(\"register-img\");' onClick='window.location.href=\"/PHP/register.php\";'>";
                  echo  "<img src='/resources/icons/clipboard.svg' alt='clipboard-icon' id='register-img' >";
                  echo  "<a href='#'>Register</a>";
                  echo "</div>";
                }
              ?>
            </li>
          </div>
        </div>
        <div class="sub-nav">
          <ul>
            <li>
              <div class="nav-element" id="active">
                <span><a href="user_account.php">Account</a></span>
              </div>
            </li>
          </ul>
        </div>
      </nav>

      <div class="page-wrapper">
        <div class="status-indicators">
          <div class="current-page-indicator">
            <a href="../index.php">Forums</a>
            <a href="#"> > </a>
            <a href="javascript:window.location.href=window.location.href" style="color:white;">User Account</a>
          </div>
        </div>
      </div>


      <!-- user image -->
      <div class="user_account">
        <div class="user_account_content">
            <div class="profile_img_container"><img class="profile_img"  src="data:image/jpeg;base64,<?php echo getUserProfileImage($conn, $_SESSION['username']); ?>" ></div>
            <!-- user info + settings -->
            <div class="profile_text_container">
            <h1>User Account</h1>
                <ul>
                  <li><p><strong><i class="fa fa-user"></i> Username :</strong>&nbsp;&nbsp; <?php echo $userInfo['username']; ?></p></li>
                  <hr>
                  <li> <p><strong><i class="fa fa-envelope"></i> Email:</strong>&nbsp;&nbsp; <?php echo $userInfo['email']; ?></p></li>
                  <label>Load Email:</label>
                  <form class="form" action="/PHP/changeEmail.php" method="post"  enctype="multipart/form-data">
                    <input type="text" name="email" placeholder="Email">
                    <input class="email_subm" type="submit" name="submit_email" value="Upload">
                  </form>
                  <hr>
                  <li> <p><strong><i class="fa fa-gear"></i> Settings:</strong></p></li>
                  <label>Select Theme:&nbsp;&nbsp;<strong>Day</strong>
                  <label class="switch"><input type="checkbox" id="checkbox"><span class="slider round"></span></label>
                  &nbsp;&nbsp;&nbsp;&nbsp;<strong>Night</strong></label>
                  <br>
                  <br>
                  <label>Load Image:</label>
                  <form class="form" action="/PHP/uploadImage.php" method="post" name="upload" enctype="multipart/form-data">
                    <input type="file" name="file" placeholder="Load file">
                    <input class="load_image" type="submit" name="submit" value="Upload">
                  </form>
                  <hr>
                </ul>
            </div>
        </div>
      </div>

      <script type ="text/javascript" src = "/JS/toggleSwitch.js"></script>



  </body>
</html>
