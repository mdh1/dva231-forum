<?php
  require_once "../PHP/databaseConnection.php";
  include "../PHP/functions.php";
  include "../PHP/forumAPI.php";
  session_start();

  $boardName = null;    // holds the name of the current forum
  $sorting = null;      // either "new" or "popular", determines how posts will be sorted
  $page = null;         // holds the current page number
  $totalPages = null;   // holds the total pages possible, based on amount of threads
  $pageSize = 10;        // set this to determine the amount of threads per page

  if(isset($_GET['forum'])) {
    $boardName = $_GET['forum'];

    if (isset($_GET['page'])) {
      $page = $_GET['page'];
    }
    else {
      $page = 1;
    }

    if (isset($_GET['sorting'])) {
      $sorting = $_GET['sorting'];
    }
    else {
      $sorting = "popular";
    }
  }

  $subforums;
  $threads;
  $threadRatings;

  if ($boardName === "all") {
    $sql = "SELECT * FROM subforums";
  }
  else {
    $sql = "SELECT * FROM subforums WHERE parentForum='$boardName'";
  }

  if ($result = mysqli_query($conn, $sql)) {
    if (mysqli_num_rows($result) > 0) {
      $i = 0;
      while ($row = mysqli_fetch_assoc($result)) {
        $subforums[$i]['name'] = "'" . $row['name'] . "'";
        $i++;
      }
      mysqli_free_result($result);
    }
    else {
      echo "Your SQL query did not match any existing objects in database.";
    }
  }

  $separatedSubforums = implode(', ', array_column($subforums, 'name'));
  $sql = "SELECT * FROM threads WHERE parentSubforum IN " . "(" . $separatedSubforums . ")" . " ORDER BY date desc";

  if ($result = mysqli_query($conn, $sql)) {
    if (mysqli_num_rows($result) > 0) {
      $i = 0;
      while ($row = mysqli_fetch_assoc($result)) {
        $threads[$i]['id'] = $row['id'];
        $threads[$i]['title'] = $row['title'];
        $threads[$i]['comment'] = $row['comment'];
        $threads[$i]['creator'] = $row['creator'];
        $threads[$i]['parentSubforum'] = $row['parentSubforum'];
        $threads[$i]['date'] = $row['date'];
        $threads[$i]['likes'] = getAmountOfThreadLikes($row['id'], $conn);
        $i++;
      }
      mysqli_free_result($result);
    }
    else {
      echo "Your SQL query did not match any existing objects in database.";
    }
  }

  if ($sorting == "popular") {
    sortThreadsByLikes($threads);
  }

  if (isset($_SESSION['username'])) {
    $threadRatings = getUserThreadRatings($conn, $_SESSION['username']);
  }

  if (is_array($threads)) {
    $totalPages = getAmountOfPages(sizeof($threads), $pageSize);
  }
 ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>forum title - home</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="shortcut icon" type="image/x-icon" href="/resources/META/favicon.png" />
    <link rel="stylesheet" href="/CSS/header.css">
    <link rel="stylesheet" href="/CSS/forum.css">
    <script type ="text/javascript" src ="/JS/functions.js" ></script>
    <script type ="text/javascript" src = "/JS/changeTheme.js"></script>
  </head>
  <body>
    <header>
      <a href="/index.php"><img src="/resources/META/banner.png" alt="logo"></a>
      <form id="search-box" action="PHP/search.php" method="post">
        <div class="search-wrapper">
          <input type="text" autocomplete="off" name="Search" placeholder="Search...">
          <img src="/resources/icons/magnifying-glass.svg" alt="magnifying-glass-icon">
        </div>
      </form>
    </header>
    <nav>
      <div class="navbar">
        <div class="main-nav">
          <div class="main-nav-left">
            <li>
              <div class="nav-element" id="active">
                <span><a href="/index.php">Forums</a></span>
              </div>
            </li>
            <li>
              <div class="nav-element">
                <span><a href="/pages/about-info.php">About</a></span>
              </div>
            </li>
          </div>

          <div class="main-nav-right">

            <li>
              <?php
               $_SESSION['previousURL']= $_SERVER['REQUEST_URI'];
                 if (isset($_SESSION['username'])) {
                   echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"profile-img\");' onmouseout='unhover(\"profile-img\");' onClick='window.location.href=\"/pages/user_account.php\";'>";
                   echo  "<img src='/resources/icons/profile.svg' alt='profile-icon' id='login-img' >";
                   echo  "<a href='#'>". $_SESSION['username'] ."</a>";
                   echo "</div>";
                 }
                else {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"login-img\");' onmouseout='unhover(\"login-img\");' onClick='window.location.href=\"/PHP/login.php\";'>";
                  echo  "<img src='/resources/icons/key.svg' alt='key-icon' id='login-img' >";
                  echo  "<a href='#'>Login</a>";
                  echo "</div>";
                }
              ?>
            </li>

            <li>
              <?php
                if (isset($_SESSION['username'])) {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"logout-img\");' onmouseout='unhover(\"logout-img\");' onClick='window.location.href=\"/PHP/logout.php\";'>";
                  echo  "<img src='/resources/icons/door.svg' alt='door-icon' id='logout-img' >";
                  echo  "<a href='#'>Logout</a>";
                  echo "</div>";
                }
                else {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"register-img\");' onmouseout='unhover(\"register-img\");' onClick='window.location.href=\"/PHP/register.php\";'>";
                  echo  "<img src='/resources/icons/clipboard.svg' alt='clipboard-icon' id='register-img' >";
                  echo  "<a href='#'>Register</a>";
                  echo "</div>";
                }
              ?>
            </li>
          </div>
        </div>
        <div class="sub-nav">
          <ul>
            <li>
              <div class="nav-element">
                <span><a href="/index.php">Home</a></span>
              </div>
            </li>
            <li>
              <?php
                if ($sorting == "popular") {
                  echo "<div class='nav-element' id='active'>";
                }
                else {
                  echo "<div class='nav-element'>";
                }
                echo "<a href='/pages/forum.php?forum=" . $boardName . "&sorting=popular&page=1'>Popular</a>";
                echo "</div>";
              ?>
            </li>
            <li>
              <?php
                if ($sorting == "new") {
                  echo "<div class='nav-element' id='active'>";
                }
                else {
                  echo "<div class='nav-element'>";
                }
                echo "<a href='/pages/forum.php?forum=" . $boardName . "&sorting=new&page=1'>New</a>";
                echo "</div>";
              ?>
            </li>
          </ul>
        </div>
      </nav>

      <div class="page-wrapper">
        <div class="status-indicators">
          <div class="status-main">
            <div class="current-page-indicator">
              <a href="../index.php">Forums</a>
              <a href="#"> > </a>
              <a href="javascript:window.location.href=window.location.href"> <?php echo $boardName; ?> </a>
              <a href="#"> > </a>
              <a href="javascript:window.location.href=window.location.href" style="color:white;">Popular</a>
            </div>
          </div>
          <div class="status-sub">
            <div class="subforum-buttons">
              <?php
                // Draws a button for each subforum, depending on which forum you're on
                foreach ($subforums as $row) {
                  $row['name'] = str_replace("'", "", $row['name']);
                  echo "<div class='subforum-button'>";
                  echo "<a href='/pages/subforum.php?subforum=" . $row['name'] . "&sorting=popular&page=1'>" . $row['name'] . "<a>";
                  echo "</div>";
                }
               ?>
            </div>
            <div class="current-subpage-indicator">
              <?php
                // Draw the page indicator buttons
                // Prevoius page button
                if ($page <= 1) {
                  echo "<div class='page-button previous-button' id='current-page'>";
                }
                else {
                  echo "<div class='page-button previous-button' onclick='window.location.href =\"/pages/forum.php?forum=" . $boardName . "&sorting=" . $sorting . "&page=" . ($page - 1) . "\"';'>";
                }
                echo "<p><</p>";
                echo "</div>";

                // Number buttons
                for ($i = 1; $i <= $totalPages; $i++) {
                  if ($i == $page) {
                    echo "<div class='page-button number-button' id='current-page'>";
                  }
                  else {
                    echo "<div class='page-button number-button' onclick='window.location.href =\"/pages/forum.php?forum=" . $boardName . "&sorting=" . $sorting . "&page=" . $i . "\"';'>";
                  }
                  echo  "<p>" . $i . "<p>";
                  echo "</div>";
                }

                // Next page button
                if ($page == $totalPages) {
                  echo "<div class='page-button next-button' id='current-page'>";
                }
                else {
                  echo "<div class='page-button next-button' onclick='window.location.href =\"/pages/forum.php?forum=" . $boardName . "&sorting=" . $sorting . "&page=" . ($page + 1) . "\"';'>";
                }
                echo "<p>></p>";
                echo "</div>";
               ?>
            </div>
          </div>
        </div>

        <div class="main-box">
          <div class="main-box-title">
            <a href="#"> <?php echo $boardName; ?> </a>
          </div>
          <div class="main-box-content">
            <?php
              // Draws all thread boxes, how many depends on $pageSize
              for ($i = ($page * $pageSize) - $pageSize; $i < ($page * $pageSize); $i++) {
                if (!($i >= sizeof($threads))) {
                  $replies = getAmountOfReplies($threads[$i]['id'], $conn);
                  $latestReply = getLatestReply($conn, $threads[$i]['id']);
                  $index = $threads[$i]['id'] * $threads[$i]['id'] * 1000; // fucking ugly, remove if possible (check out getUserThreadRatings())
                  echo "<div class='main-box-content-element'>";
                  echo  "<div class='main-box-content-element-main'>";
                  echo    "<div id='likes-container'>";
                  if (in_array($threads[$i]['id'], array_column($threadRatings, 'id'))) { //if liked or disliked
                    if ($threadRatings[$index]['type'] === "liked") { // if liked
                      echo "<div class='like-button' id='active' onclick='rateThread(\"" . $_SESSION['username'] . "\", \"" . $threads[$i]['id'] . "\", \"unrate\")'>";
                      echo  "<img src='/resources/icons/up-arrow.svg' alt='up-arrow-icon'>";
                      echo "</div>";
                      echo "<div class='dislike-button' onclick='rateThread(\"" . $_SESSION['username'] . "\", \"" . $threads[$i]['id'] . "\", \"unrateToDislike\")'>";
                      echo  "<img src='/resources/icons/down-arrow.svg' alt='up-arrow-icon'>";
                      echo "</div>";
                    }
                    else { // if disliked
                      echo "<div class='like-button'  onclick='rateThread(\"" . $_SESSION['username'] . "\", \"" . $threads[$i]['id'] . "\", \"unrateToLike\")'>";
                      echo  "<img src='/resources/icons/up-arrow.svg' alt='up-arrow-icon'>";
                      echo "</div>";
                      echo "<div class='dislike-button' id='active' onclick='rateThread(\"" . $_SESSION['username'] . "\", \"" . $threads[$i]['id'] . "\", \"unrate\")'>";
                      echo  "<img src='/resources/icons/down-arrow.svg' alt='up-arrow-icon'>";
                      echo "</div>";
                    }
                  }
                  else { // if neither liked of disliked
                    echo "<div class='like-button' onclick='rateThread(\"" . $_SESSION['username'] . "\", \"" . $threads[$i]['id'] . "\", \"like\")'>";
                    echo  "<img src='/resources/icons/up-arrow.svg' alt='up-arrow-icon'>";
                    echo "</div>";
                    echo "<div class='dislike-button' onclick='rateThread(\"" . $_SESSION['username'] . "\", \"" . $threads[$i]['id'] . "\", \"dislike\")'>";
                    echo  "<img src='/resources/icons/down-arrow.svg' alt='up-arrow-icon'>";
                    echo "</div>";
                  }
                  echo    "</div>";
                  echo    "<div id='container'>";
                  echo      "<a href='/pages/thread.php?subforum=" . $threads[$i]['parentSubforum'] . "&threadId=" . $threads[$i]['id'] . "' class='title'>" . $threads[$i]['title'] . "</a>";
                  echo      "<br>";
                  echo      "<div class='latest-reply-container'>";
                  echo        "<img src='/resources/icons/clock.svg' alt='clock-icon'>";
                  echo        "<a href='#' class='latest-reply'>[Latest reply] " . $latestReply['comment'] . "</a>";
                  echo      "</div>";
                  echo    "</div>";
                  echo    "<div class='amount-of-likes'>";
                  echo      "<p>Likes</p>";
                  echo      "<p id='replies'>" . $threads[$i]['likes'] . "</p>";
                  echo    "</div>";
                  echo    "<div class='amount-of-replies'>";
                  echo      "<p>Replies</p>";
                  echo      "<p id='replies'>" . $replies . "</p>";
                  echo    "</div>";
                  echo  "</div>";
                  echo  "<div class='main-box-content-element-status'>";
                  echo    "<div id='container'>";
                  echo      "<a href='#'>" . $threads[$i]['creator'] . "</a>";
                  echo      "<p>" . $threads[$i]['parentSubforum'] . "</p>";
                  echo      "<p>" . $threads[$i]['date'] . "</p>";
                  echo    "</div>";
                  echo  "</div>";
                  echo "</div>";

                }
              }
             ?>
          </div>
        </div>
      </div>

      <script type="text/javascript">
        function rateThread(username, threadId, ratingType) {
          $.ajax({
            url: "/PHP/rateThread.php",
            type: "POST",
            data: {user:username, id:threadId, type:ratingType},
            success: function(data) {
              console.log(data);
              location.reload();
            }
          });
        }
      </script>

  </body>
</html>
