<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>forum title - About</title>
    <link rel="shortcut icon" type="image/x-icon" href="../resources/META/favicon.png" />
    <link rel="stylesheet" href="../CSS/master.css">
  </head>
  <body>
    <header>
        <a href="index.php"><img src="../resources/META/banner.png" alt="logo"></a>
      </header>
      <nav>
        <div class="navbar">
          <div class="main-nav">
            <div class="main-nav-left">
              <li>
                <div class="nav-element">
                  <span><a href="../index.php">Forums</a></span>
                </div>
              </li>
              <li>
                <div class="nav-element" id="active">
                  <span><a href="/pages/about-contact.php">About</a></span>
                </div>
              </li>
            </div>

            <div class="main-nav-right">
              <li>
                <div class="nav-element">
                  <span><a href="../PHP/login.php">Login</a></span>
                </div>
              </li>
              <li>
                <div class="nav-element">
                  <span><a href="../PHP/register.php">Register</a></span>
                </div>
              </li>
            </div>
          </div>

          <div class="sub-nav">
            <ul>
              <li>
                <div class="nav-element">
                  <span><a href="about-info.php">Info</a></span>
                </div>
              </li>
              <li>
                <div class="nav-element" id="active">
                  <span><a href="about-contact.php">Contact</a></span>
                </div>
              </li>
            </ul>
          </div>

  </body>
</html>
