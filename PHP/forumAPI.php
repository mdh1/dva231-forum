<?php
function getAmountOfReplies($parentThreadId, $conn)
{
  $amount = 0;
  $sql = "SELECT COUNT(*) AS count FROM posts WHERE parentThread="."'". $parentThreadId ."'"." LIMIT 1";
  if ($result = mysqli_query($conn, $sql)) {
    $resultRow = mysqli_fetch_assoc($result);
    $amount = $resultRow['count'];
    mysqli_free_result($result);
  }
  return $amount;
}

function getAmountOfThreadLikes($parentThreadId, $conn)
{
  $likes = 0;
  $disliked = 0;
  $sql = "SELECT COUNT(*) AS count FROM threadRatings WHERE id="."'". $parentThreadId ."'"." AND type='liked' LIMIT 1";
  if ($result = mysqli_query($conn, $sql)) {
    $resultRow = mysqli_fetch_assoc($result);
    $likes = $resultRow['count'];
    mysqli_free_result($result);
  }
  $sql = "SELECT COUNT(*) AS count FROM threadRatings WHERE id="."'". $parentThreadId ."'"." AND type='disliked' LIMIT 1";
  if ($result = mysqli_query($conn, $sql)) {
    $resultRow = mysqli_fetch_assoc($result);
    $disliked = $resultRow['count'];
    mysqli_free_result($result);
  }
  return $likes - $disliked;
}

function getUserThreadRatings($conn, $username) {
  $threadRatings;
  $sql = "SELECT * FROM threadRatings WHERE user='$username' ORDER BY id asc";

  if (!$result = mysqli_query($conn, $sql)) {
    return;
  }
  if (mysqli_num_rows($result) <= 0) {
    return;
  }

  $i = 0;
  while ($row = mysqli_fetch_assoc($result)) {
    $threadRatings[$i]['id'] = $row['id'];
    $threadRatings[$i]['type'] = $row['type'];
    $i++;
  }
  mysqli_free_result($result);

  // REMOVE IF POSSIBLE
  // restructure array so that id and type is on index=id*id*1000 (ugly solution)
  // Currently, if thread id ever exceed 2000, we're fucked
  for ($i = 0; $i < sizeof($threadRatings); $i++) {
    $index = $threadRatings[$i]['id'] * $threadRatings[$i]['id'] * 1000;
    $threadRatings[$index]['id'] = $threadRatings[$i]['id'];
    $threadRatings[$index]['type'] = $threadRatings[$i]['type'];
  }

  return $threadRatings;
}

// sorts $threads array from forum/subforum by likes in descending order
function sortThreadsByLikes(&$array)
{
  if (is_array($array)) {
    usort($array, function ($a, $b){
      return strcasecmp($b['likes'], $a['likes']);
    });
  }
}

function getAmountOfPages($amountOfThreads, $pageSize) {
  return ceil($amountOfThreads/$pageSize);
}

function getLatestReply($conn, $threadId) {
  $thread = array(
    'id' => null,
    'comment' => null
  );

  $sql = "SELECT id, comment FROM posts WHERE parentThread="."'". $threadId ."'"." ORDER BY date desc LIMIT 1";
  if ($result = mysqli_query($conn, $sql)) {
    $resultRow = mysqli_fetch_assoc($result);
    $thread['id'] = $resultRow['id'];
    $thread['comment'] = $resultRow['comment'];
    mysqli_free_result($result);
  }
  return $thread;
}

function getSubforumFromParent($conn, $parentForum, &$array, $result)
{
  $i = 0;
  while ($row = mysqli_fetch_assoc($result))
  {
    $array[$i]['name'] = "'" . $row['name'] . "'";
    $array[$i]['parentForum'] = "'" . $row['parentForum'] . "'";
    $array[$i]['amountOfThreads'] = 0;
    $i++;
  }
}

function getSubforumInfo($conn, $parentForum, &$array)
{
  // fetch all subforum in $parentForum
  $sql = "SELECT * FROM subforums WHERE parentForum='$parentForum'";
  if (!$result = mysqli_query($conn, $sql))
  {
    die();
  }
  if (!mysqli_num_rows($result) > 0)
  {
    die();
  }
  getSubforumFromParent($conn, $parentForum, $array, $result);
  mysqli_free_result($result);

  // fetch the amount of threads in $parentsubforum
  $i = 0;
  foreach ($array as $row)
  {
    $row['name'] = str_replace("'", "", $row['name']);
    $sql = "SELECT COUNT(*) AS count FROM threads WHERE parentSubforum="."'".$row['name']."'"." LIMIT 1";
    if (!$result = mysqli_query($conn, $sql))
    {
      die();
    }
    if (mysqli_num_rows($result) <= 0)
    {
      die();
    }
    $resultRow = mysqli_fetch_assoc($result);
    $array[$i++]['amountOfThreads'] = $resultRow['count'];
    mysqli_free_result($result);
  }

  // fetch title and id for the most recent thread in $parentForum
  $i = 0;
  foreach ($array as $row)
  {
    $row['name'] = str_replace("'", "", $row['name']);
    $sql = "SELECT title, id FROM threads WHERE parentSubforum="."'".$row['name']."'"." ORDER BY date desc LIMIT 1";
    if (!$result = mysqli_query($conn, $sql))
    {
      die();
    }
    if (mysqli_num_rows($result) <= 0)
    {
      $array[$i]['latestThread'] = "";
      $array[$i++]['threadId'] = "";
      continue;
    }
    $resultRow = mysqli_fetch_assoc($result);
    $array[$i]['latestThread'] = $resultRow['title'];
    $array[$i++]['threadId'] = $resultRow['id'];
    mysqli_free_result($result);
  }
}

function getQuestions($conn, $parentForum, &$array)
{
  // fetch all subforum in $parentForum
  $sql = "SELECT * FROM subforums WHERE parentForum='$parentForum'";
  if ($result = mysqli_query($conn, $sql)) {
    if (mysqli_num_rows($result) > 0) {
      $i = 0;
      while ($row = mysqli_fetch_assoc($result)) {
        $array[$i]['name'] = "'" . $row['name'] . "'";
        $array[$i]['parentForum'] = "'" . $row['parentForum'] . "'";
        $array[$i]['amountOfThreads'] = 0;
        $i++;
      }
      mysqli_free_result($result);
    }
    else {
      echo "Your SQL query did not match any existing objects in database.";
    }
  }

  $i = 0;
  foreach ($array as $row) {
    $row['name'] = str_replace("'", "", $row['name']);
    $sql = "SELECT title, id FROM threads WHERE parentSubforum="."'".$row['name']."'"." ORDER BY date desc LIMIT 3";
    if ($result = mysqli_query($conn, $sql)) {
      if (mysqli_num_rows($result) > 0) {
        while ($resultRow = mysqli_fetch_assoc($result)){
        $array[$i]['name'] = "'" . $row['name'] . "'";
        $array[$i]['question'] = $resultRow['title'];
        $array[$i++]['threadId'] = $resultRow['id'];
        }
      mysqli_free_result($result);
      }
      else {
        $array[$i]['latestThread'] = "";
        $array[$i++]['threadId'] = "";
      }
    }
  }
}

function getRecentActivity($conn, &$array)
{
  $threadIds;
  $sql = "SELECT parentThread FROM posts ORDER BY date desc LIMIT 10";
  if ($result = mysqli_query($conn, $sql)) {
    if (mysqli_num_rows($result) > 0) {
      $i = 0;
      while ($row = mysqli_fetch_assoc($result)) {
        $threadIds[$i]['id'] = $row['parentThread'];
        $i++;
      }
      mysqli_free_result($result);
    }
    else {
      echo "Your SQL query did not match any existing objects in database.";
    }
  }
  $separatedThreads = implode(', ', array_column($threadIds, 'id'));
  $sql = "SELECT * FROM threads WHERE id IN " . "(" . $separatedThreads . ") LIMIT 7";
  if (!$result = mysqli_query($conn, $sql)) {
    return error;
  }
  else if (mysqli_num_rows($result) <= 0) {
    return error;
  }
  $i=0;
  while ($row = mysqli_fetch_assoc($result)) {
    $array[$i]['id'] = $row['id'];
    $array[$i]['title'] = $row['title'];
    $array[$i]['comment'] = $row['comment'];
    $array[$i]['creator'] = $row['creator'];
    $array[$i]['parentSubforum'] = $row['parentSubforum'];
    $array[$i]['date'] = $row['date'];
    $i++;
  }
}

function drawForumBoxes($array) {
  foreach ($array as $row) {
    $row['name'] = str_replace("'", "", $row['name']);
    echo "<div class='main-box-content-element'>";
    echo  "<div class='main-box-content-element-main'>";
    echo      "<div id='container'>";
    echo        "<a href='/pages/subforum.php?subforum=" . $row['name'] . "&sorting=popular&page=1' class='title'>" . $row['name'] . "</a>";
    echo        "<br>";
    echo    "</div>";
    echo    "<div class='amount-of-threads'>";
    echo      "<p>Threads</p>";
    echo      "<p id='number'>" . $row['amountOfThreads'] . "</p>";
    echo    "</div>";
    echo  "</div>";
    echo  "<div class='main-box-content-element-status'>";
    echo    "<div id='container'>";
    echo      "<div id='latest-thread-container'>";
    echo        "<img src='/resources/icons/clock.svg' alt='clock-icon' id='latest-thread-img'>";
    echo        "<p> [Latest thread] </p>";
    echo      "</div>";
    echo      "<a href='/pages/thread.php?subforum=" . $row['name'] . "&threadId=" . $row['threadId'] . "' class='title'>" . $row['latestThread'] . "</a>";
    echo    "</div>";
    echo  "</div>";
    echo "</div>";
  }
}
// In order to user getUserProfileImage-function use following HTML image tag:
/*<img src="data:image/jpeg;base64,<?php echo getUserProfileImage($conn, $username); ?>" width="x" height="y" /> */
function getUserProfileImage($conn, $username)
{
  //$username = $_SESSION['username'];
  $sql = "SELECT picture FROM userbase WHERE username = '$username'";
  $result = mysqli_query($conn, $sql);
  $image = mysqli_fetch_object($result);

  return $image->picture;
}
/*
function getUserProfileImage($conn)
{
  $username = $_SESSION['username'];
  $sql = "SELECT picture FROM userbase WHERE username = '$username'";
  $result = mysqli_query($conn, $sql);
  mysqli_fetch_object($result)

  return mysqli_fetch_assoc($result);
}
*/
?>
