<?php
  require_once "PHP/databaseConnection.php";
  include "../PHP/functions.php";
  include "../PHP/forumAPI.php";

  $boardName = null;    // holds the name of the current forum
  $sorting = null;      // either "new" or "popular", determines how posts will be sorted
  $page = null;         // holds the current page number
  $totalPages = null;   // holds the total pages possible, based on amount of threads
  $pageSize = 10;        // set this to determine the amount of threads per page

  if(isset($_GET['forum'])) {
    $boardName = $_GET['forum'];
    session_start();
    $_SESSION['currentBoard'] = $_GET['forum'];

    if (isset($_GET['page'])) {
      $page = $_GET['page'];
    }
    else {
      $page = 1;
    }

    if (isset($_GET['sorting'])) {
      $sorting = $_GET['sorting'];
    }
    else {
      $sorting = "popular";
    }
  }

  $subforums[] = array(
    'name' => null
  );
  $threads[] = array(
    'id' => null,
    'title' => null,
    'comment' => null,
    'creator' => null,
    'parentSubforum' => null,
    'date' => null,
    'likes' => null,
  );

  if ($boardName === "all") {
    $sql = "SELECT * FROM subforums";
  }
  else {
    $sql = "SELECT * FROM subforums WHERE parentForum='$boardName'";
  }

  if ($result = mysqli_query($conn, $sql)) {
    if (mysqli_num_rows($result) > 0) {
      $i = 0;
      while ($row = mysqli_fetch_assoc($result)) {
        $subforums[$i]['name'] = "'" . $row['name'] . "'";
        $i++;
      }
      mysqli_free_result($result);
    }
    else {
      echo "Your SQL query did not match any existing objects in database.";
    }
  }

  $separatedSubforums = implode(', ', array_column($subforums, 'name'));
  $sql = "SELECT * FROM threads WHERE parentSubforum IN " . "(" . $separatedSubforums . ")" . " ORDER BY date desc";

  if ($result = mysqli_query($conn, $sql)) {
    if (mysqli_num_rows($result) > 0) {
      $i = 0;
      while ($row = mysqli_fetch_assoc($result)) {
        $threads[$i]['id'] = $row['id'];
        $threads[$i]['title'] = $row['title'];
        $threads[$i]['comment'] = $row['comment'];
        $threads[$i]['creator'] = $row['creator'];
        $threads[$i]['parentSubforum'] = $row['parentSubforum'];
        $threads[$i]['date'] = $row['date'];
        $threads[$i]['likes'] = getAmountOfThreadLikes($row['id'], $conn);
        $i++;
      }
      mysqli_free_result($result);
    }
    else {
      echo "Your SQL query did not match any existing objects in database.";
    }
  }

  if ($sorting == "popular") {
    sortThreadsByLikes($threads);
  }

  $totalPages = getAmountOfPages(sizeof($threads), $pageSize);

  if (isset($_POST['submit-search'])) {
    $search = mysqli_real_escape_string($conn, $_POST['search']);
    $sql = "SELECT * FROM threads WHERE title LIKE '%$search%'";
    $result = mysqli_query($conn, $sql);
    $queryResult = mysqli_num_rows($result);

    if ($queryResult > 0) {
      while ($row = mysqli_fetch_assoc($result)) {

      }
    }
    else {
      echo "Your SQL query did not match any existing objects in database.";
    }
  }

?>

 <!DOCTYPE html>
 <html lang="en" dir="ltr">
   <head>
     <meta charset="utf-8">
     <title>forum title - home</title>
     <link rel="shortcut icon" type="image/x-icon" href="/resources/META/favicon.png" />
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
     <link rel="stylesheet" href="/CSS/header.css">
     <link rel="stylesheet" href="/CSS/forum.css">
   </head>
   <body>
     <header>
       <a href="index.php"><img src="/resources/META/banner.png" alt="logo"></a>
       <form id="search-box" action="/PHP/search.php" method="post">
         <input type="text" name="Search" placeholder="Search...">
       </form>
     </header>
     <nav>
       <div class="navbar">
         <div class="main-nav">
           <div class="main-nav-left">
             <li>
               <div class="nav-element" id="active">
                 <span><a href="index.php">Forums</a></span>
               </div>
             </li>
             <li>
               <div class="nav-element">
                 <span><a href="pages/about-info.php">About</a></span>
               </div>
             </li>
           </div>

           <div class="main-nav-right">

             <li>
               <div class="nav-element" style="cursor: pointer;" onmouseover="hover('login-img');" onmouseout="unhover('login-img');" onClick="window.open('/PHP/login.php');">
                 <img src="/resources/icons/key.svg" alt="key-icon" id="login-img" >
                 <a href="#"> <?php if (isset($_SESSION['username'])) { echo $_SESSION['username']; } else { echo "Login";} ?>
               </a>
               </div>
             </li>

             <li>
               <div class="nav-element" style="cursor: pointer;" onmouseover="hover('register-img');" onmouseout="unhover('register-img');" onClick="window.open('/PHP/register.php');">
                 <img src="/resources/icons/clipboard.svg" alt="clipboard-icon" id="register-img">
                 <a href="#">Register</a>
               </div>
             </li>
           </div>
         </div>
         <div class="sub-nav">
           <ul>
             <li>
               <div class="nav-element">
                 <span><a href="/index.php">Home</a></span>
               </div>
             </li>
             <li>
               <?php
                 if ($sorting == "popular") {
                   echo "<div class='nav-element' id='active'>";
                 }
                 else {
                   echo "<div class='nav-element'>";
                 }
                 echo "<a href='/pages/forum.php?forum=" . $boardName . "&sorting=popular&page=1'>Popular</a>";
                 echo "</div>";
               ?>
             </li>
             <li>
               <?php
                 if ($sorting == "new") {
                   echo "<div class='nav-element' id='active'>";
                 }
                 else {
                   echo "<div class='nav-element'>";
                 }
                 echo "<a href='/pages/forum.php?forum=" . $boardName . "&sorting=new&page=1'>New</a>";
                 echo "</div>";
               ?>
             </li>
           </ul>
         </div>
       </nav>

       <div class="page-wrapper">
         <div class="status-indicators">
           <div class="status-main">
             <div class="current-page-indicator">
               <a href="../index.php">Forums</a>
               <a href="#"> > </a>
               <a href="forum.php?forum=<?php echo $boardName; ?>&sorting=popular"> <?php echo $boardName; ?> </a>
               <a href="#"> > </a>
               <a href="javascript:window.location.href=window.location.href" style="color:white;">Popular</a>
             </div>
           </div>
           <div class="status-sub">
             <div class="subforum-buttons">
               <?php
                 // Draws a button for each subforum, depending on which forum you're on
                 foreach ($subforums as $row) {
                   $row['name'] = str_replace("'", "", $row['name']);
                   echo "<div class='subforum-button'>";
                   echo "<a href='/pages/subforum.php?subforum=" . $row['name'] . "&sorting=popular&page=1'>" . $row['name'] . "<a>";
                   echo "</div>";
                 }
                ?>
             </div>
             <div class="current-subpage-indicator">
               <?php
                 // Draw the page indicator buttons
                 // Prevoius page button
                 if ($page <= 1) {
                   echo "<div class='page-button previous-button' id='current-page'>";
                 }
                 else {
                   echo "<div class='page-button previous-button' onclick='window.location.href =\"/pages/forum.php?forum=" . $boardName . "&sorting=" . $sorting . "&page=" . ($page - 1) . "\"';'>";
                 }
                 echo "<p><</p>";
                 echo "</div>";

                 // Number buttons
                 for ($i = 1; $i <= $totalPages; $i++) {
                   if ($i == $page) {
                     echo "<div class='page-button number-button' id='current-page'>";
                   }
                   else {
                     echo "<div class='page-button number-button' onclick='window.location.href =\"/pages/forum.php?forum=" . $boardName . "&sorting=" . $sorting . "&page=" . $i . "\"';'>";
                   }
                   echo  "<p>" . $i . "<p>";
                   echo "</div>";
                 }

                 // Next page button
                 if ($page == $totalPages) {
                   echo "<div class='page-button next-button' id='current-page'>";
                 }
                 else {
                   echo "<div class='page-button next-button' onclick='window.location.href =\"/pages/forum.php?forum=" . $boardName . "&sorting=" . $sorting . "&page=" . ($page + 1) . "\"';'>";
                 }
                 echo "<p>></p>";
                 echo "</div>";
                ?>
             </div>
           </div>
         </div>

         <div class="main-box">
           <div class="main-box-title">
             <a href="#"> <?php echo $boardName; ?> </a>
           </div>
           <div class="main-box-content">
             <?php
               // Draws all thread boxes, how many depends on $pageSize
               for ($i = ($page * $pageSize) - $pageSize; $i < ($page * $pageSize); $i++) {
                 if (!($i >= sizeof($threads))) {
                   $replies = getAmountOfReplies($threads[$i]['id'], $conn);
                   $latestReply = getLatestReply($conn, $threads[$i]['id']);
                   echo "<div class='main-box-content-element'>";
                   echo  "<div class='main-box-content-element-main'>";
                   echo    "<div id='likes-container'>";
                   echo      "<div class='like-button'>";
                   echo        "<img src='/resources/icons/up-arrow.svg' alt='up-arrow-icon'>";
                   echo      "</div>";
                   echo      "<div class='dislike-button'>";
                   echo        "<img src='/resources/icons/down-arrow.svg' alt='up-arrow-icon'>";
                   echo      "</div>";
                   echo    "</div>";
                   echo    "<div id='container'>";
                   echo      "<a href='/pages/thread.php?subforum=" . $threads[$i]['parentSubforum'] . "&threadId=" . $threads[$i]['id'] . "' class='title'>" . $threads[$i]['title'] . "</a>";
                   echo      "<br>";
                   echo      "<div class='latest-reply-container'>";
                   echo        "<img src='/resources/icons/clock.svg' alt='clock-icon'>";
                   echo        "<a href='#' class='latest-reply'>[Latest reply] " . $latestReply['comment'] . "</a>";
                   echo      "</div>";
                   echo    "</div>";
                   echo    "<div class='amount-of-likes'>";
                   echo      "<p>Likes</p>";
                   echo      "<p id='replies'>" . $threads[$i]['likes'] . "</p>";
                   echo    "</div>";
                   echo    "<div class='amount-of-replies'>";
                   echo      "<p>Replies</p>";
                   echo      "<p id='replies'>" . $replies . "</p>";
                   echo    "</div>";
                   echo  "</div>";
                   echo  "<div class='main-box-content-element-status'>";
                   echo    "<div id='container'>";
                   echo      "<a href='#'>" . $threads[$i]['creator'] . "</a>";
                   echo      "<p>" . $threads[$i]['parentSubforum'] . "</p>";
                   echo      "<p>" . $threads[$i]['date'] . "</p>";
                   echo    "</div>";
                   echo  "</div>";
                   echo "</div>";
                 }
               }
              ?>
           </div>
         </div>
       </div>

   </body>
 </html>
