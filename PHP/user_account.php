<?php
require_once "functions.php";
require_once "databaseConnection.php";
require_once "forumAPI.php";
session_start();
 ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>forum title - home</title>
    <link rel="shortcut icon" type="image/x-icon" href="/resources/META/favicon.png" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/CSS/header.css">
    <link rel="stylesheet" href="/CSS/forum.css">
    <link rel="stylesheet" href="/CSS/user_account.css">
  </head>
  <body>
    <header>
      <a href="/index.php"><img src="/resources/META/banner.png" alt="logo"></a>
      <form id="search-box" action="PHP/search.php" method="post">
        <div class="search-wrapper">
          <input type="text" autocomplete="off" name="Search" placeholder="Search...">
          <img src="/resources/icons/magnifying-glass.svg" alt="magnifying-glass-icon">
        </div>
      </form>
    </header>
    <nav>
      <div class="navbar">
        <div class="main-nav">
          <div class="main-nav-left">
            <li>
              <div class="nav-element">
                <span><a href="/index.php">Forums</a></span>
              </div>
            </li>
            <li>
              <div class="nav-element">
                <span><a href="/pages/about-info.php">About</a></span>
              </div>
            </li>
          </div>

          <div class="main-nav-right">

            <li>
              <?php
                if (isset($_SESSION['username'])) {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"login-img\");' onmouseout='unhover(\"login-img\");' onClick='window.location.href=\"/PHP/user_account.php\";'>";
                  echo  "<img src='/resources/icons/key.svg' alt='key-icon' id='login-img' >";
                  echo  "<a href='#'>". $_SESSION['username'] ."</a>";
                  echo "</div>";
                }
                else {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"login-img\");' onmouseout='unhover(\"login-img\");' onClick='window.location.href=\"/PHP/login.php\";'>";
                  echo  "<img src='/resources/icons/key.svg' alt='key-icon' id='login-img' >";
                  echo  "<a href='#'>Login</a>";
                  echo "</div>";
                }
              ?>
            </li>

            <li>
              <?php
                if (isset($_SESSION['username'])) {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"register-img\");' onmouseout='unhover(\"register-img\");' onClick='window.location.href=\"/PHP/logout.php\";'>";
                  echo  "<img src='/resources/icons/clipboard.svg' alt='clipboard-icon' id='register-img' >";
                  echo  "<a href='#'>Logout</a>";
                  echo "</div>";
                }
                else {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"register-img\");' onmouseout='unhover(\"register-img\");' onClick='window.location.href=\"/PHP/register.php\";'>";
                  echo  "<img src='/resources/icons/clipboard.svg' alt='clipboard-icon' id='register-img' >";
                  echo  "<a href='#'>Register</a>";
                  echo "</div>";
                }
              ?>
            </li>
          </div>
        </div>
        <div class="sub-nav">
          <ul>
            <li>
              <div class="nav-element" id="active">
                <span><a href="/PHP/user_account.php">Account</a></span>
              </div>
            </li>
          </ul>
        </div>
      </nav>

      <div class="page-wrapper">
        <div class="status-indicators">
          <div class="current-page-indicator">
            <a href="javascript:window.location.href=window.location.href">Forums</a>
            <a href="#"> > </a>
            <a href="javascript:window.location.href=window.location.href" style="color:white;">User Account</a>
          </div>
        </div>
      </div>

      <!-- user image -->
      <div class="user_account">
        <div class="user_account_content">
            <div class="profile_img_container"><img class="profile_img" src="data:image/jpeg;base64,<?php echo getUserProfileImage($conn, $_SESSION['username']); ?>" width="450" height="450"></img></div>
            <!-- user info + settings -->
            <div class="profile_text_container">
                <h1>User Account</h1>
                <ul>
                  <li><pre class="fa fa-user" ><strong> Username :</strong>     SissiCR</pre></li>
                  <hr>
                  <li> <pre class="fa fa-envelope"><strong> Email:</strong>            hej@forum.se</pre></li>
                  <hr>
                  <li> <pre class="fa fa-gear"><strong> Settings:</strong></pre></li>
                  <label>Load Image:</label>&nbsp;&nbsp;&nbsp;&nbsp;
                  <form action="/PHP/uploadImage.php" method="post" name="upload" enctype="multipart/form-data">
                    <input type="file" name="file" placeholder="Load file">
                    <input type="submit" name="submit" value="Upload">
                  </form>
                  <label>Select Theme:&nbsp;&nbsp;<strong>Day</strong>
                  <label class="switch"><input type="checkbox" id="checkbox"> <span class="slider round"></span></label>
                  <strong>Night</strong></label>
                  <hr>
                </ul>
            </div>
        </div>
      </div>

      <script>

          /*Creates a variable for the toogle switch element*/
          const toggleSwitch = document.querySelector('.switch input[type="checkbox"]');

          const currentTheme = localStorage.getItem('theme');
          /*sets the theme from the previously saved theme*/
          if (currentTheme) {
              document.documentElement.setAttribute('data-theme', currentTheme);

              if (currentTheme === 'dark') {
                  toggleSwitch.checked = true;
                  var toggleStatus = toggleSwitch.checked;

              }
          }

          /*when the switch is checked the theme is set to dark else light + stores
          the current theme in the web browser localstorage*/
          function switchTheme(e) {
              if (e.target.checked) {
                document.documentElement.setAttribute('data-theme', 'dark');
                  localStorage.setItem('theme', 'dark');
              }
              else {
                document.documentElement.setAttribute('data-theme', 'light');
                localStorage.setItem('theme', 'light');
              }
          }

          /*listens when the toogle switch is change and calls switchtheme(), 'change' is
          the name of the event on the event interface*/
          toggleSwitch.addEventListener('change', switchTheme, false);

      </script>
  </body>
</html>
