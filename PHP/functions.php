<?php
function userNameExists($conn, $username)
{
  $response = mysqli_query($conn, "SELECT username FROM userbase WHERE username = '".$username."'");
  return mysqli_num_rows($response) > 0;
}

function authenticateUser($conn, $username)
{
  if(!$_SESSION['login'])
  {
    return false;
  }

  $retrieveToken = mysqli_query($conn, "SELECT token FROM userTokens WHERE username = '".$username."'
  ORDER BY timedate DESC LIMIT 1");

  $db = mysqli_fetch_object($retrieveToken);
  return password_verify(getToken(), $db->token);
}

function getToken()
{
  $userIP = file_get_contents('http://bot.whatismyipaddress.com/'); //$_SERVER['REMOTE_ADDR']
  $userGeoLoc = @unserialize(file_get_contents('http://ip-api.com/php/'.$userIP));
  return $userGeoLoc['regionName'] . $userGeoLoc['isp'];
}

function isAdmin($conn, $username)
{
  $sql = mysqli_query($conn, "SELECT privilege FROM userbase WHERE username = '".$username."'");
  $db = mysqli_fetch_object($sql);
  return $db->privilege == 'admin';
}
?>
