
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Temp - Register account</title>
    <link rel="stylesheet" href="/CSS/register.css">
    <link rel="stylesheet" href="/CSS/header.css">
    <link rel="stylesheet" href="/CSS/index.css">
    <link rel="shortcut icon" type="image/x-icon" href="resources/META/favicon.png" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type = "text/javascript" src = "/JS/functions.js" ></script>
    <script type ="text/javascript" src = "/JS/changeTheme.js"></script>
  </head>
  <body>
  <header>
      <a href="/index.php"><img src="/resources/META/banner.png" alt="logo"></a>
      <form id="search-box" action="/PHP/search.php" method="post">
        <input type="text" name="Search" placeholder="Search...">
      </form>
    </header>
    <nav>
      <div class="navbar">
        <div class="main-nav">
          <div class="main-nav-left">
            <li>
              <div class="nav-element">
                <span><a href="/index.php">Forums</a></span>
              </div>
            </li>
            <li>
              <div class="nav-element">
                <span><a href="/pages/about-info.php">About</a></span>
              </div>
            </li>
          </div>

          <div class="main-nav-right">

            <li>
              <div class="nav-element" style="cursor: pointer;" onmouseover="hover('login-img');" onmouseout="unhover('login-img');" onClick="window.open('/PHP/login.php');">
                <img src="/resources/icons/key.svg" alt="key-icon" id="login-img" >
                <a href="#"> <?php if (isset($_SESSION['username'])) { echo $_SESSION['username']; } else { echo "Login";} ?>
              </a>
              </div>
            </li>

            <li>
              <div class="nav-element" style="cursor: pointer;" onmouseover="hover('register-img');" onmouseout="unhover('register-img');" onClick="window.open('/PHP/register.php');">
                <img src="/resources/icons/clipboard.svg" alt="clipboard-icon" id="register-img">
                <a href="#">Register</a>
              </div>
            </li>
          </div>
        </div>
        <div class="sub-nav">
          <ul>
            <li>
              <div class="nav-element" id="active">
                <span><a href="#">Register</a></span>
              </div>
            </li>
          </ul>
        </div>
      </nav>

      <div class="page-wrapper">
        <div class="status-indicators">
          <div class="current-page-indicator">
            <a href="../index.php">Forums</a>
            <a href="#"> > </a>
            <a href="javascript:window.location.href=window.location.href" style="color:white;">Home</a>
          </div>
        </div>

  <div class="section">
    <div class="loginForm">
    <div>
        <h1 class="h1">REGISTER<h1>
      </div>
      <form action="register.php" method="POST" enctype="multipart/form-data">
        <br><input class="user_input" type="text" name="username" placeholder="Username" required><br><br>
        <input class ="passw_input" type="password" name="password" placeholder="Password" required><br><br>
        <input class ="passw_input" type="password" name="confirmPassword" placeholder="confirm password" required><br><br>
        <input class="log_button"  type="submit" name="submit" value="Register">
        <div>
        <p><a class="register_link" href="/PHP/login.php" >Already an user? Login</a></p>
        <?php
require_once "databaseConnection.php";
require_once "functions.php";

if (empty($_POST["submit"]) || empty($_POST["username"]) || empty($_POST["password"]))
{
  die();
}

if ($_POST["password"] != $_POST["confirmPassword"])
{
  echo "Passwords do not match";
  unset($_POST["password"]);
  unset($_POST["confirmPassword"]);
  die();
}

$username = mysqli_real_escape_string($conn, $_POST["username"]);
$password = mysqli_real_escape_string($conn, $_POST["password"]);

if (userNameExists($conn, $_POST["username"]))
{
  echo "User already exists.";
  die();
}

$hashedPassword = password_hash($password, PASSWORD_DEFAULT);

$date = date("Y-m-d");
$sql = "CALL addUser('$username', '$hashedPassword', 'user', 'noEmail', '$date')";

if ($conn->query($sql))
{
  $_SESSION['previousURL']= $_SERVER['REQUEST_URI'];
  header("location: /PHP/login.php ");
  echo "$username has been created, Welcome to the Forum";
  die();
}
else {
  echo "Account creation failure.";
  echo "Error code: . $conn->error .";
  die();
}

?>
        </div>
      </form>
    </div>
</div>

  </body>
</html>
