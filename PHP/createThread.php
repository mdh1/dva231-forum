<?php
  require_once "databaseConnection.php";
  require_once "functions.php";
  session_start();

  if (!authenticateUser($conn, $_SESSION['username']))
  {
    header('Location: http://localhost/PHP/login.php');
  }

  // Check these values since they are required to upload to database
  if (empty($_POST['title']) || empty($_POST['content']) || !(isset($_GET['subforum']))) {
    // Return to previous page
    header('Location: /pages/subforum.php?subforum=' . $_GET['subforum'] . '&sorting=' . $_GET['sorting'] . '&page=' . $_GET['page'] . '');
    die();
  }

  $getId = mysqli_query($conn, "SELECT id FROM threads ORDER BY date desc LIMIT 1");
  $row = mysqli_fetch_assoc($getId);
  $id = $row['id'] + 1;

  $title = mysqli_real_escape_string($conn, $_POST['title']);
  $content = mysqli_real_escape_string($conn, $_POST['content']);
  $subforum = mysqli_real_escape_string($conn, $_GET['subforum']);
  $username = mysqli_real_escape_string($conn, $_SESSION['username']);
  $date = date("Y-m-d H:i:s");

  $sql = "CALL createThread('$id', '$title', '$content', '$username', '$subforum', '$date')";
  mysqli_query($conn, $sql);

  header('Location: /pages/subforum.php?subforum='.$_GET['subforum'].'&sorting=new&page=1');
 ?>
