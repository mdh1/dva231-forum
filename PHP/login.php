<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Temp - Login</title>
    <link rel="shortcut icon" type="image/x-icon" href="resources/META/favicon.png" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/CSS/login.css">
    <link rel="stylesheet" href="/CSS/header.css">
    <link rel="stylesheet" href="/CSS/index.css">
    <script type = "text/javascript" src = "/JS/functions.js" ></script>
    <script type ="text/javascript" src = "/JS/changeTheme.js"></script>
  </head>
  <body>

    <header>
      <a href="/index.php"><img src="/resources/META/banner.png" alt="logo"></a>
      <form id="search-box" action="PHP/search.php" method="post">
        <div class="search-wrapper">
          <input type="text" autocomplete="off" name="Search" placeholder="Search...">
          <img src="/resources/icons/magnifying-glass.svg" alt="magnifying-glass-icon">
        </div>
      </form>
    </header>
    <nav>
      <div class="navbar">
        <div class="main-nav">
          <div class="main-nav-left">
            <li>
              <div class="nav-element">
                <span><a href="/index.php">Forums</a></span>
              </div>
            </li>
            <li>
              <div class="nav-element">
                <span><a href="/pages/about-info.php">About</a></span>
              </div>
            </li>
          </div>

          <div class="main-nav-right">

            <li>
              <?php
                if (isset($_SESSION['username'])) { //Unnecessary code, can't be logged in when logging in? If it's staying change "/PHP/user_account.php" to "../pages/user_account.php"
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"login-img\");' onmouseout='unhover(\"login-img\");' onClick='window.location.href=\"/PHP/user_account.php\";'>";
                  echo  "<img src='/resources/icons/key.svg' alt='key-icon' id='login-img' >";
                  echo  "<a href='#'>". $_SESSION['username'] ."</a>";
                  echo "</div>";
                }
                else {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"login-img\");' onmouseout='unhover(\"login-img\");' onClick='window.location.href=\"/PHP/login.php\";'>";
                  echo  "<img src='/resources/icons/key.svg' alt='key-icon' id='login-img' >";
                  echo  "<a href='#'>Login</a>";
                  echo "</div>";
                }
              ?>
            </li>

            <li>
              <?php
                if (isset($_SESSION['username'])) {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"register-img\");' onmouseout='unhover(\"register-img\");' onClick='window.location.href=\"/PHP/logout.php\";'>";
                  echo  "<img src='/resources/icons/clipboard.svg' alt='clipboard-icon' id='register-img' >";
                  echo  "<a href='#'>Logout</a>";
                  echo "</div>";
                }
                else {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"register-img\");' onmouseout='unhover(\"register-img\");' onClick='window.location.href=\"/PHP/register.php\";'>";
                  echo  "<img src='/resources/icons/clipboard.svg' alt='clipboard-icon' id='register-img' >";
                  echo  "<a href='#'>Register</a>";
                  echo "</div>";
                }
              ?>
            </li>
          </div>
        </div>
        <div class="sub-nav">
          <ul>
            <li>
              <div class="nav-element" id="active">
                <span><a href="/PHP/login.php">Login</a></span>
              </div>
            </li>
          </ul>
        </div>
      </nav>

      <div class="page-wrapper">
        <div class="status-indicators">
          <div class="current-page-indicator">
            <a href="../index.php">Forums</a>
            <a href="#"> > </a>
            <a href="login.php" style="color:white;">Login</a>
          </div>
        </div>

    <div class="section">
<div class="loginForm">
      <div>
        <h1 class="h1">LOGIN<h1>
      </div>
      <form action="login.php" method="POST" enctype="multipart/form-data">
        <br><input class="user_input" type="text" name="username" placeholder="Username" required><br><br>
        <input class ="passw_input" type="password" name="password" placeholder="Password" required><br><br>
        <input class="log_button" type="submit" name="submit" value="Login">
        <div>
           <p><a class="register_link" href="/PHP/register.php" >Not an user yet? Register</a></p>
           <?php
require_once "functions.php";
require_once "databaseConnection.php";
session_start();
if (empty($_POST["submit"]))
{
  die();
}

$username = mysqli_real_escape_string($conn, $_POST["username"]);
$password = mysqli_real_escape_string($conn, $_POST["password"]);

if (!userNameExists($conn, $username))
{
  echo "User does not exists on this forum";
  die();
}

$retrieveHashedPassword = mysqli_query($conn, "SELECT password FROM userbase WHERE username = '".$username."'");
$hashed = mysqli_fetch_object($retrieveHashedPassword);

if (password_verify($password, $hashed->password))
{
  $date = date("Y-m-d H:i:s");

  $hashedtoken = password_hash(getToken(), PASSWORD_DEFAULT);
  $sql = "INSERT INTO userTokens (username, token, timedate) VALUES ('".$username."', '".$hashedtoken."', '".$date."')";
  $conn->query($sql);

  $_SESSION['login'] = true;
  $_SESSION['username'] = $username;
}

if (!authenticateUser($conn, $username))
{
  echo "<p>Authentication failure. Session authenticity not confirmed.</p>";
}
echo "Authentication successful.";
$previousPage= $_SESSION['previousURL'];
header("location: $previousPage ");
?>

        </div>
      </form>
    </div>

</div>
  </body>
</html>
