<?php
require_once "databaseConnection.php";
require_once "functions.php";
session_start();
?>
<?php
  $boardName = null;

  if(isset($_GET['forum'])) {
    $boardName = $_GET['forum'];
    $_SESSION['currentBoard'] = $_GET['forum'];
  }

  $subforums[] = array(
    'name' => null
  );
  $threads[] = array(
    'id' => null,
    'title' => null,
    'comment' => null,
    'creator' => null,
    'parentSubforum' => null,
    'date' => null,
  );

  $sql = "SELECT * FROM subforums WHERE parentForum='Other'";

  if ($result = mysqli_query($conn, $sql)) {
    if (mysqli_num_rows($result) > 0) {
      $i = 0;
      while ($row = mysqli_fetch_assoc($result)) {
        $subforums[$i]['name'] = "'" . $row['name'] . "'";
        $i++;
      }
      mysqli_free_result($result);
    }
    else {
      echo ".$boardname.";
    }
  }

  $separatedSubforums = implode(', ', array_column($subforums, 'name'));
  $sql = "SELECT * FROM threads WHERE parentSubforum IN " . "(" . $separatedSubforums . ")" . "";

  if ($result = mysqli_query($conn, $sql)) {
    if (mysqli_num_rows($result) > 0) {
      $i = 0;
      while ($row = mysqli_fetch_assoc($result)) {
        $threads[$i]['id'] = $row['id'];
        $threads[$i]['title'] = $row['title'];
        $threads[$i]['comment'] = $row['comment'];
        $threads[$i]['creator'] = $row['creator'];
        $threads[$i]['parentSubforum'] = $row['parentSubforum'];
        $threads[$i]['date'] = $row['date'];
        $i++;
      }
      mysqli_free_result($result);
    }
    else {
      echo "fuck it";
    }
  }
 ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>forum title - home</title>
    <link rel="shortcut icon" type="image/x-icon" href="/resources/META/favicon.png" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/CSS/header.css">
    <link rel="stylesheet" href="/CSS/forum.css">
  </head>
  <body>
    <header>
      <a href="index.php"><img src="/resources/META/banner.png" alt="logo"></a>
    </header>
    <nav>
      <div class="navbar">
        <div class="main-nav">
          <div class="main-nav-left">
            <li>
              <div class="nav-element" id="active">
                <span><a href="/index.php">Forums</a></span>
              </div>
            </li>
            <li>
              <div class="nav-element">
                <span><a href="/pages/about-info.php">About</a></span>
              </div>
            </li>
          </div>

          <div class="main-nav-right">
            <li>
              <div class="nav-element">
                <span><a href="PHP/login.php">Login</a></span>
              </div>
            </li>
            <li>
              <div class="nav-element">
                <span><a href="PHP/register.php">Register</a></span>
              </div>
            </li>
          </div>
        </div>

        <div class="sub-nav">
          <ul>
            <li>
              <div class="nav-element">
                <span><a href="/index.php">Home</a></span>
              </div>
            </li>
            <li>
              <div class="nav-element" id="active">
                <span><a href="#">Popular</a></span>
              </div>
            </li>
            <li>
              <div class="nav-element">
                <span><a href="/pages/board-new.php">New</a></span>
              </div>
            </li>
          </ul>
        </div>
      </nav>

      <div class="page-wrapper">
        <div class="status-indicators">
          <div class="status-main">
            <div class="current-page-indicator">
              <a href="../index.php">Forums</a>
              <a href="#"> > </a>
              <a href="forum.php?forum=<?php echo $boardName; ?>&sorting=popular"> <?php echo $boardName; ?> </a>
              <a href="#"> > </a>
              <a href="javascript:window.location.href=window.location.href" style="color:white;">Popular</a>
            </div>
          </div>

        </div>

        <div class="main-box">
          <div class="main-box-title">
            <a href="#"> <?php echo $boardName; ?> </a>
            <?php
              $username = $_SESSION['username'];
              echo "hejaheja";
              echo $_SESSION['username'];
             if(isAdmin($conn, $username))
             {
               $hejaheja = 'createThread.php';
                echo "<div class='create-thread-button' onclick='window.location.href = \"http://localhost/PHP/createThread.php\";'>";
                echo "<p>Create Thread</p>";
                echo "</div>";
            }
          ?>
          </div>
          <div class="main-box-content">
            <?php
              foreach ($threads as $row) {
                echo "<div class='main-box-content-element'>";
                echo  "<div class='main-box-content-element-main'>";
                echo      "<div id='container'>";
                echo        "<a href='/pages/thread.php?subforum=" . $row['parentSubforum'] . "&threadId=" . $row['id'] . "' class='title'>" . $row['title'] . "</a>";
                echo        "<br>";
                echo        "<a href='#' class='latest-reply'>latestReply</a>";
                echo    "</div>";
                echo    "<div class='amount-of-likes'>";
                echo      "<p>Likes</p>";
                echo      "<p id='replies'>10</p>";
                echo    "</div>";
                echo    "<div class='amount-of-replies'>";
                echo      "<p>Replies</p>";
                echo      "<p id='replies'>60</p>";
                echo    "</div>";
                echo  "</div>";
                echo  "<div class='main-box-content-element-status'>";
                echo    "<div id='container'>";
                echo      "<a href='#'>" . $row['creator'] . "</a>";
                echo      "<p>" . $row['parentSubforum'] . "</p>";
                echo      "<p>" . $row['date'] . "</p>";
                echo    "</div>";
                echo  "</div>";
                echo "</div>";
              }
             ?>
          </div>
        </div>
      </div>

  </body>
</html>
