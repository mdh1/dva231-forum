/*Creates a variable for the toogle switch element*/
const toggleSwitch = document.querySelector('.switch input[type="checkbox"]'); 

const currentTheme = localStorage.getItem('theme');
/*sets the theme from the previously saved theme*/
if (currentTheme) {
    document.documentElement.setAttribute('data-theme', currentTheme);
  
    if (currentTheme === 'dark') {
        toggleSwitch.checked = true;
        var toggleStatus = toggleSwitch.checked;
    }
}

/*listens when the toogle switch is change and calls switchtheme(), 'change' is 
the name of the event on the event interface*/
toggleSwitch.addEventListener('change', switchTheme, false);