//function for dropdown login menu
$(document).ready(function(){
var arrow= $(".arrow-up");
var form= $(".login-form");
var status= false;
$("#login").click(function(event){
  event.preventDefault();
  if(status == false){
    arrow.fadeIn();
    form.fadeIn();
    status= true;
  }
  else{
    arrow.fadeOut();
    form.fadeOut();
    status= false;
  }
})
})
//function for dropdown register menu
$(document).ready(function(){
var r_arrow= $(".r-arrow-up");
var r_form= $(".register-form");
var r_status= false;
$("#register").click(function(event){
  event.preventDefault();
  if(r_status == false){
    r_arrow.fadeIn();
    r_form.fadeIn();
    r_status= true;
  }
  else{
    r_arrow.fadeOut();
    r_form.fadeOut();
    r_status= false;
  }
})
})

function hover(element) {
  if (element == "login-img") {
    document.getElementById(element).src = "/resources/icons/key-highlight.svg";
  }
  else if (element == "register-img") {
    document.getElementById(element).src = "/resources/icons/clipboard-highlight.svg";
  }
  else if (element == "logout-img") {
    document.getElementById(element).src = "/resources/icons/door-highlight.svg";
  }
  else if (element == "profile-img") {
    document.getElementById(element).src = "/resources/icons/profile-highlight.svg";
  }
}

function unhover(element) {
  if (element == "login-img") {
    document.getElementById(element).src = "/resources/icons/key.svg";
  }
  else if (element == "register-img") {
    document.getElementById(element).src = "/resources/icons/clipboard.svg";
  }
  else if (element == "logout-img") {
    document.getElementById(element).src = "/resources/icons/door.svg";
  }
  else if (element == "profile-img") {
    document.getElementById(element).src = "/resources/icons/profile.svg";
  }
}

function switchTheme(e) {
  if (e.target.checked) {
      document.documentElement.setAttribute('data-theme', 'dark');
      localStorage.setItem('theme', 'dark');
  }
  else {
      document.documentElement.setAttribute('data-theme', 'light');
      localStorage.setItem('theme', 'light');
  }
}
