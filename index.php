<?php
require_once "PHP/databaseConnection.php";
include "PHP/functions.php";
include "PHP/forumAPI.php";
session_start();

$movieSubforums[] = array(
  'name' => null,
  'parentForum' => null,
  'amountOfThreads' => null,
  'latestThread' => null,
  'threadId' => null
);

$showsSubforums[] = array(
  'name' => null,
  'parentForum' => null,
  'amountOfThreads' => null,
  'latestThread' => null,
  'threadId' => null
);

$productionSubforums[] = array(
  'name' => null,
  'parentForum' => null,
  'amountOfThreads' => null,
  'latestThread' => null,
  'threadId' => null
);

$questionOfTheWeek[] = array(
  'name' => null,
  'parentForum' => null,
  'question' => null,
  'replies' => null,
  'threadId' => null
);

$recentActivity;

getSubforumInfo($conn, 'Movies', $movieSubforums);
getSubforumInfo($conn, 'Shows', $showsSubforums);
getSubforumInfo($conn ,'Production', $productionSubforums);
getQuestions($conn, 'Other', $questionOfTheWeek);
getRecentActivity($conn, $recentActivity);

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>forum title - home</title>
    <link rel="shortcut icon" type="image/x-icon" href="resources/META/favicon.png" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="CSS/header.css">
    <link rel="stylesheet" href="CSS/index.css">
    <script type = "text/javascript" src = "/JS/functions.js" ></script>
    <script type = "text/javascript" src = "/JS/changeTheme.js" ></script>
  </head>
  <body>
    <header>
      <a href="index.php"><img src="/resources/META/banner.png" alt="logo"></a>
      <form id="search-box" action="PHP/search.php" method="post">
        <div class="search-wrapper">
          <input type="text" autocomplete="off" name="Search" placeholder="Search...">
          <img src="/resources/icons/magnifying-glass.svg" alt="magnifying-glass-icon">
        </div>
      </form>
    </header>
    <nav>
      <div class="navbar">
        <div class="main-nav">
          <div class="main-nav-left">
            <li>
              <div class="nav-element" id="active">
                <span><a href="/index.php">Forums</a></span>
              </div>
            </li>
            <li>
              <div class="nav-element">
                <span><a href="/pages/about-info.php">About</a></span>
              </div>
            </li>
          </div>

          <div class="main-nav-right">

            <li>
              <?php
               $_SESSION['previousURL']= $_SERVER['REQUEST_URI'];
                if (isset($_SESSION['username'])) {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"profile-img\");' onmouseout='unhover(\"profile-img\");' onClick='window.location.href=\"/pages/user_account.php\";'>";
                  echo  "<img src='/resources/icons/profile.svg' alt='profile-icon' id='login-img' >";
                  echo  "<a href='#'>". $_SESSION['username'] ."</a>";
                  echo "</div>";
                }
                else {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"login-img\");' onmouseout='unhover(\"login-img\");' onClick='window.location.href=\"/PHP/login.php\";'>";
                  echo  "<img src='/resources/icons/key.svg' alt='key-icon' id='login-img' >";
                  echo  "<a href='#'>Login</a>";
                  echo "</div>";
                }
              ?>
            </li>

            <li>
              <?php
                if (isset($_SESSION['username'])) {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"logout-img\");' onmouseout='unhover(\"logout-img\");' onClick='window.location.href=\"/PHP/logout.php\";'>";
                  echo  "<img src='/resources/icons/door.svg' alt='door-icon' id='logout-img' >";
                  echo  "<a href='#'>Logout</a>";
                  echo "</div>";
                }
                else {
                  echo "<div class='nav-element' style='cursor: pointer;' onmouseover='hover(\"register-img\");' onmouseout='unhover(\"register-img\");' onClick='window.location.href=\"/PHP/register.php\";'>";
                  echo  "<img src='/resources/icons/clipboard.svg' alt='clipboard-icon' id='register-img' >";
                  echo  "<a href='#'>Register</a>";
                  echo "</div>";
                }
              ?>
            </li>
          </div>
        </div>
        <div class="sub-nav">
          <ul>
            <li>
              <div class="nav-element" id="active">
                <span><a href="/index.php">Home</a></span>
              </div>
            </li>
            <li>
              <div class="nav-element">
                <div class="dropdown">
                  <a href="/pages/forum.php?forum=all&sorting=popular&page=1">Popular</a>
                </div>
              </div>
            </li>
            <li>
              <div class="nav-element">
                <div class="dropdown">
                  <a href="/pages/forum.php?forum=all&sorting=new&page=1">New</a>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </nav>

      <div class="page-wrapper">
        <div class="status-indicators">
          <div class="current-page-indicator">
            <a href="javascript:window.location.href=window.location.href">Forums</a>
            <a href="#"> > </a>
            <a href="javascript:window.location.href=window.location.href" style="color:white;">Home</a>
          </div>
        </div>
        <div class="page-wrapper-left">
          <div class="page-left-content page-board1">
            <div class="page-left-content-title">
              <a href="/pages/forum.php?forum=movies&sorting=popular">Movies</a>
            </div>
            <div class="page-left-content-wrapper">
              <?php
                drawForumBoxes($movieSubforums);
               ?>
            </div>
          </div>
          <div class="page-left-content page-board2">
            <div class="page-left-content-title">
              <a href="pages/forum.php?forum=shows">Shows</a>
            </div>
            <div class="page-left-content-wrapper">
              <?php
                drawForumBoxes($showsSubforums);
               ?>
            </div>
          </div>
          <div class="page-left-content page-board3">
            <div class="page-left-content-title">
              <a href="pages/forum.php?forum=production">Production</a>
            </div>
            <div class="page-left-content-wrapper">
              <?php
                drawForumBoxes($productionSubforums);
               ?>
            </div>
          </div>
        </div>
        <div class="page-wrapper-right">
          <div class="page-right-content questionOfTheWeek">
            <div class="page-right-content-title">
              <a href="/PHP/weeklyQuestion.php?forum=Question of the week">Question of the Week</a>
            </div>
            <div class="page-right-content-wrapper">
              <?php
              // Draw the questions in the question of the week box
              foreach ($questionOfTheWeek as $row) {
                $row['name'] = str_replace("'", "", $row['name']);
                echo "<div class='main-box-content-element'>";
                echo  "<div class='main-box-content-element-main'>";
                echo      "<div id='container'>";
                echo        "<a href='/pages/thread.php?subforum=" . $row['name'] . "&threadId=" . $row['threadId'] . "' class='title'>" . $row['question'] . "</a>";
                echo      "</div>";
                echo  "</div>";
                echo  "<div class='main-box-content-element-status'>";
                echo    "<div id='answer-container'>";
                echo      "<p> Comments: " . getAmountOfReplies($row['threadId'], $conn) . " </p>";
                echo    "</div>";
                echo  "</div>";
                echo "</div>";
              }
             ?>
            </div>
          </div>
          <div class="page-right-content recentActivity">
            <div class="page-right-content-title">
              <a href="#">Recent Activity</a>
            </div>
            <div class="page-right-content-wrapper">
              <?php
              // Draw the questions in the question of the week box
              foreach ($recentActivity as $row) {
                $row['title'] = str_replace("'", "", $row['title']);
                echo "<div class='main-box-content-element'>";
                echo  "<div class='main-box-content-element-main'>";
                echo      "<div id='container'>";
                echo        "<a href='/pages/thread.php?subforum=" . $row['parentSubforum'] . "&threadId=" . $row['id'] . "' class='title'>" . $row['title'] . "</a>";
                echo      "</div>";
                echo  "</div>";
                echo  "<div class='main-box-content-element-status'>";
                echo    "<div id='answer-container'>";
                echo      "<p> Comments: " . getAmountOfReplies($row['id'], $conn) . " </p>";
                echo    "</div>";
                echo  "</div>";
                echo "</div>";
              }
             ?>
            </div>
          </div>
        </div>
      </div>

  </body>
</html>
